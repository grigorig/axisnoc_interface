library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity axisnoc_interface_v1_0 is
    generic (
        -- Users to add parameters here
        NVC : integer := 4;
        NVC_WIDTH : integer := 2;

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- Parameters of Axi Master Bus Interface M00_AXIS
        C_M00_AXIS_TDATA_WIDTH    : integer    := 32;

        -- Parameters of Axi Slave Bus Interface S00_AXIS
        C_S00_AXIS_TDATA_WIDTH    : integer    := 32
    );
    port (
        -- Users to add ports here

        -- User ports ends
        -- Do not modify the ports beyond this line
        m00_noc_data  : in STD_LOGIC_VECTOR(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
        m00_noc_vc    : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        m00_noc_valid : in STD_LOGIC;
        m00_noc_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);

        s00_noc_data  : out STD_LOGIC_VECTOR(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
        s00_noc_vc    : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        s00_noc_valid : out STD_LOGIC;
        s00_noc_ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);

        -- Ports of Axi Master Bus Interface M00_AXIS
        m00_axis_aclk    : in std_logic;
        m00_axis_aresetn    : in std_logic;
        m00_axis_tvalid    : out std_logic;
        m00_axis_tdata    : out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
        m00_axis_tlast    : out std_logic;
        m00_axis_tready    : in std_logic;

        -- Ports of Axi Slave Bus Interface S00_AXIS
        s00_axis_aclk    : in std_logic;
        s00_axis_aresetn    : in std_logic;
        s00_axis_tready    : out std_logic;
        s00_axis_tdata    : in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
        s00_axis_tlast    : in std_logic;
        s00_axis_tvalid    : in std_logic
    );
end axisnoc_interface_v1_0;

architecture arch_imp of axisnoc_interface_v1_0 is

    -- component declaration
    component axisnoc_interface_v1_0_M00_AXIS is
        generic (
        NVC : integer := 4;
        NVC_WIDTH : integer := 2;
        C_M_AXIS_TDATA_WIDTH    : integer    := 32
        );
        port (
        noc_data  : in STD_LOGIC_VECTOR(C_M_AXIS_TDATA_WIDTH-1 downto 0);
        noc_vc    : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        noc_valid : in STD_LOGIC;
        noc_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);
        M_AXIS_ACLK    : in std_logic;
        M_AXIS_ARESETN    : in std_logic;
        M_AXIS_TVALID    : out std_logic;
        M_AXIS_TDATA    : out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
        M_AXIS_TLAST    : out std_logic;
        M_AXIS_TREADY    : in std_logic
        );
    end component axisnoc_interface_v1_0_M00_AXIS;

    component axisnoc_interface_v1_0_S00_AXIS is
        generic (
        NVC : integer := 4;
        NVC_WIDTH : integer := 2;
        C_S_AXIS_TDATA_WIDTH    : integer    := 32
        );
        port (
        noc_data  : out STD_LOGIC_VECTOR(C_S_AXIS_TDATA_WIDTH-1 downto 0);
        noc_vc    : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        noc_valid : out STD_LOGIC;
        noc_ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);
        S_AXIS_ACLK    : in std_logic;
        S_AXIS_ARESETN    : in std_logic;
        S_AXIS_TREADY    : out std_logic;
        S_AXIS_TDATA    : in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
        S_AXIS_TLAST    : in std_logic;
        S_AXIS_TVALID    : in std_logic
        );
    end component axisnoc_interface_v1_0_S00_AXIS;

begin

-- Instantiation of Axi Bus Interface M00_AXIS
axisnoc_interface_v1_0_M00_AXIS_inst : axisnoc_interface_v1_0_M00_AXIS
    generic map (
        NVC => NVC,
        NVC_WIDTH => NVC_WIDTH,
        C_M_AXIS_TDATA_WIDTH    => C_M00_AXIS_TDATA_WIDTH
    )
    port map (
        noc_data => m00_noc_data,
        noc_vc => m00_noc_vc,
        noc_valid => m00_noc_valid,
        noc_ready => m00_noc_ready,
        M_AXIS_ACLK    => m00_axis_aclk,
        M_AXIS_ARESETN    => m00_axis_aresetn,
        M_AXIS_TVALID    => m00_axis_tvalid,
        M_AXIS_TDATA    => m00_axis_tdata,
        M_AXIS_TLAST    => m00_axis_tlast,
        M_AXIS_TREADY    => m00_axis_tready
    );

-- Instantiation of Axi Bus Interface S00_AXIS
axisnoc_interface_v1_0_S00_AXIS_inst : axisnoc_interface_v1_0_S00_AXIS
    generic map (
        NVC => NVC,
        NVC_WIDTH => NVC_WIDTH,
        C_S_AXIS_TDATA_WIDTH    => C_S00_AXIS_TDATA_WIDTH
    )
    port map (
        noc_data => s00_noc_data,
        noc_vc => s00_noc_vc,
        noc_valid => s00_noc_valid,
        noc_ready => s00_noc_ready,
        S_AXIS_ACLK    => s00_axis_aclk,
        S_AXIS_ARESETN    => s00_axis_aresetn,
        S_AXIS_TREADY    => s00_axis_tready,
        S_AXIS_TDATA    => s00_axis_tdata,
        S_AXIS_TLAST    => s00_axis_tlast,
        S_AXIS_TVALID    => s00_axis_tvalid
    );

    -- Add user logic here

    -- User logic ends

end arch_imp;
