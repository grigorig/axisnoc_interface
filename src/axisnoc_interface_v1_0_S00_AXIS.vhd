library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity axisnoc_interface_v1_0_S00_AXIS is
    generic (
        -- Users to add parameters here
        NVC : integer := 4;
        NVC_WIDTH : integer := 2;
        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- AXI4Stream sink: Data Width
        C_S_AXIS_TDATA_WIDTH    : integer    := 32
    );
    port (
        -- Users to add ports here

        -- connect to NoC input port
        noc_data  : out STD_LOGIC_VECTOR(C_S_AXIS_TDATA_WIDTH-1 downto 0);
        noc_vc    : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        noc_valid : out STD_LOGIC;
        noc_ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);

        -- User ports ends
        -- Do not modify the ports beyond this line

        -- AXI4Stream sink: Clock
        S_AXIS_ACLK    : in std_logic;
        -- AXI4Stream sink: Reset
        S_AXIS_ARESETN    : in std_logic;
        -- Ready to accept data in
        S_AXIS_TREADY    : out std_logic;
        -- Data in
        S_AXIS_TDATA    : in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
        -- Indicates boundary of last packet
        S_AXIS_TLAST    : in std_logic;
        -- Data is in valid
        S_AXIS_TVALID    : in std_logic
    );
end axisnoc_interface_v1_0_S00_AXIS;

architecture arch_imp of axisnoc_interface_v1_0_S00_AXIS is
begin

    noc_data      <= S_AXIS_TDATA;
    noc_vc        <= (others => '0');
    noc_valid     <= S_AXIS_TVALID;
    S_AXIS_TREADY <= noc_ready(0);

end arch_imp;
