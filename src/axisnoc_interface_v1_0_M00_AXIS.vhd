library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity axisnoc_interface_v1_0_M00_AXIS is
	generic (
		-- Users to add parameters here
        NVC : integer := 4;
        NVC_WIDTH : integer := 2;
		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
		C_M_AXIS_TDATA_WIDTH	: integer	:= 32
	);
	port (
		-- Users to add ports here

        -- connect to NoC output port
		noc_data  : in STD_LOGIC_VECTOR(C_M_AXIS_TDATA_WIDTH-1 downto 0);
		noc_vc    : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
		noc_valid : in STD_LOGIC;
		noc_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);

		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global ports
		M_AXIS_ACLK	: in std_logic;
		-- 
		M_AXIS_ARESETN	: in std_logic;
		-- Master Stream Ports. TVALID indicates that the master is driving a valid transfer, A transfer takes place when both TVALID and TREADY are asserted. 
		M_AXIS_TVALID	: out std_logic;
		-- TDATA is the primary payload that is used to provide the data that is passing across the interface from the master.
		M_AXIS_TDATA	: out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
		-- TLAST indicates the boundary of a packet.
		M_AXIS_TLAST	: out std_logic;
		-- TREADY indicates that the slave can accept a transfer in the current cycle.
		M_AXIS_TREADY	: in std_logic
	);
end axisnoc_interface_v1_0_M00_AXIS;

architecture implementation of axisnoc_interface_v1_0_M00_AXIS is
begin

    M_AXIS_TDATA    <= noc_data;
    M_AXIS_TLAST    <= '0';
    M_AXIS_TVALID   <= noc_valid;
    noc_ready       <= (NVC-1 downto 1 => '0') & M_AXIS_TREADY;

end implementation;
